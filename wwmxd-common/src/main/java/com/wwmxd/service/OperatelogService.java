package com.wwmxd.service;

import com.wwmxd.entity.Operatelog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author WWMXD
 * @since 2018-03-02 16:25:03
 */
public interface OperatelogService extends  IService<Operatelog> {
	
}
